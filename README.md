# crxn zones

DNS zones

## Create domain entry

### Choose a domain

Choose a domain which ends with `.crxn`.

There are two ways to create the domain:
- You host the name servers yourself. For this you install a DNS daemon on your server and make it available to the general public for queries about your domain.
- You enter the reouce records directly in the zone file.

### Self host dns

You need one or more IP addresses from your DNS server. You enter them according to the following scheme:
```
test                                    IN      NS      ns1.test
test                                    IN      NS      ns2.test
ns1.test                                IN      AAAA    fd00:b00b:b00b:53::1
ns2.test                                IN      AAAA    fd00:b00b:b00b:53::2
```
You replace `test` with your domain. Replace the IP addresses with the addresses of your DNS servers or your DNS server. You can add more or less nameservers accordingly. It is common to use `ns1`, `ns2`, `ns3`, ... as prefix. But this is not mandatory.

### Direct entries

This saves you the trouble of having to run a DNS server yourself, but you are not as flexible with changes and have to set a PR every time.

The following are sample entries:
```
test                                    IN      AAAA    fd00:b00b:b00b::1
home.test                               IN      AAAA    fd00:b00b:b00b:37ef::5
mobile.test                             IN      AAAA    fd00:b00b:b00b:9362::5
```

### Format

To make the zone file look uniform, there are some rules:

- Always enter new domains at the end of the file or if you already have domains after them.
- Leave one line blank above your entries.
- Write two lines above your entry your name, organization, or other unique identifier.
- Leave two blank lines above it.
- Always leave one line blank at the end of the file.

## Root servers wanted!

1) There are several ways to become a root server. The easiest is to perform a TSIG secured zone transfer from ns1.crxn. and then mirror this zone. This has to be arranged manually with Marek Küthe.
2) Another possibility is to sign and publish the zone itself. This requires DNSKEY keys to be set on all root servers. This has to be agreed with Marek Küthe and the CRXN community.
